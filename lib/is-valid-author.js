const authorsStrategy = require('../authors-strategy')
const ANY = '*'

// context: {
//   root: Msg,
//   graph: TangleGraph,
//   tips: [{ key: MsgId, T }]
// }
// author: FeedId | *
// seq: Integer | UnixTime

module.exports = function isValidAuthor (context, author, seq) {
  isValidAuthor.error = null

  const tips = context.tips
    .map(tip => tip.T.authors) // each tip has a translation state T, with T.authors,
    .map(authorsStrategy.mapToOutput) // which we reify of parse more easily

  const isOurAuthorValid = isValidForAllTips(tips, author, seq)
  const error = isValidForAllTips.error /// <<< here

  if (isOurAuthorValid) return true
  else {
    if (isInitialAuthor(context, author) && !findActiveAuthor(tips, seq)) {
      return true
    }

    isValidAuthor.error = error
    return false
  }
}

function isValidForAllTips (tips, author, seq) {
  isValidForAllTips.error = null
  return tips.every(state => {
    // state: {
    //   [authorId]: [Interval]
    // }
    // Interval: {
    //   start: Integer,
    //   end: Integer | null
    // }
    if (state[ANY]) {
      if (isActiveAuthor(state, ANY)) return true
      isValidForAllTips.error = new Error('Invalid author: no current permission for (*)')
      // should be gone?
    }

    if (state[author]) {
      if (isActiveAuthor(state, author, seq)) return true
      isValidForAllTips.error = new Error('Invalid author: your permission is invalid on one or more tips')
    }

    if (!isValidForAllTips.error) {
      isValidForAllTips.error = new Error('Invalid author: you do not have permission to edit')
    }

    return false
  })
}

function isActiveAuthor (state, author, seq) {
  if (!(author in state)) return false

  if (author === ANY) seq = Number(Date.now())
  if (!seq) {
    const intervals = state[author]
    return intervals[intervals.length - 1].end === null
  }

  return state[author].some(({ start, end }) => {
    return (
      (seq >= start) &&
      (end === null || seq <= end)
    )
  })
}

function isInitialAuthor (context, author) {
  return context.root.value.author === author
}

function findActiveAuthor (tips, seq) {
  const allAuthors = new Set(
    tips.flatMap(state => Object.keys(state))
  )
  allAuthors.delete(ANY)

  return [...allAuthors]
    .find(author => {
      return isValidForAllTips(tips, author)
    })
}
