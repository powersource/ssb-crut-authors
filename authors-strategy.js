const ComplexSet = require('@tangle/complex-set')
const Defns = require('ssb-schema-definitions')

module.exports = ComplexSet(`^(\\*|${Defns().feedId.pattern})$`)
