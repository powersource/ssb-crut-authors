/*
  read is mainly tested alongside other rich scenarios:
  - see create.test.js which uses read
  - see update.test.js which uses use of compare-read.js
*/

// TODO
// - where a root message had no author, but it's fine
// - where an invalid update is manually published
