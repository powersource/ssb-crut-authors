module.exports = {
  replicate: require('scuttle-testbot').replicate,
  compareRead: require('./compare-read'),
  SSB: require('./ssb'),
  Spec: require('./spec'),
  fullState (record) {
    const state = { ...record.states[0] }
    delete state.key

    return {
      ...record,
      ...state,
      conflictFields: []
    }
  }
}
