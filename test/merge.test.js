const test = require('tape')
const { promisify } = require('util')

const Crut = require('../')
const { SSB, Spec, replicate, compareRead, fullState } = require('./helpers')

test('merge (simultaneously remove feedId as they update)', { objectPrintDepth: 8 }, async t => {
  const alice = SSB()
  alice.name = 'alice'
  alice.wiki = new Crut(alice, Spec())

  const bob = SSB()
  bob.name = 'bob'
  bob.wiki = new Crut(bob, Spec())

  const id = await alice.wiki.create({
    body: 'dinosaur notes',
    authors: { add: [alice.id, bob.id] }
  })
  t.true(id, 'alice creates a record with authors: [alice, bob]')
  await replicate({ from: alice, to: bob })

  await alice.wiki.update(id, { authors: { remove: [bob.id] } })
    .then(updateId => t.true(updateId, 'alice removes bob from authors'))
  // Alice removes Bob, but their update is not replicated (yet)

  await bob.wiki.update(id, { body: '<3' })
    .then(updateId => t.true(updateId, 'bob simultaneously publishes an update'))

  await replicate({ from: bob, to: alice })
  await replicate({ from: alice, to: bob })

  const record = await alice.wiki.read(id)
  t.equal(record.states.length, 2, 'there is a forked state')

  const err = await bob.wiki.update(id, { body: '):' })
    .catch(err => err)
  t.match(err.message, /Invalid author/, 'bob can no longer edit')

  const updateId = await alice.wiki.update(id, {})
    .catch(t.error)
  t.true(updateId, 'alice can merge tips')
  // alice can merge tips

  /* testing #get */
  const expected = fullState({
    key: id,
    type: 'wiki',
    originalAuthor: alice.id,
    recps: null,
    states: [{
      key: updateId,
      authors: {
        [alice.id]: [{ start: 0, end: null }]
      },
      body: '<3',
      tombstone: null
    }]
  })
  // NOTE this means that Bob's edit while he's out of sync is STILL VALID
  // as he was extending on a valid state, in a valid way

  // Somewhere strangely, he's not mentioned in the authors
  // because his values are "anihilated" by adding anti-values of the same amount

  await compareRead(alice, bob, id, expected, t)

  alice.close()
  bob.close()
  t.end()
})

test('merge (both authors removes the other)', async t => {
  const alice = SSB()
  alice.name = 'alice'
  alice.wiki = new Crut(alice, Spec())

  const bob = SSB()
  bob.name = 'bob'
  bob.wiki = new Crut(bob, Spec())

  const id = await alice.wiki.create({
    body: 'dinosaur notes',
    authors: { add: [alice.id, bob.id] }
  })
  t.ok(id, 'alice adds self + bob as authors')

  await replicate({ from: alice, to: bob })

  const removeBobMsgId = await alice.wiki.update(id, { authors: { remove: [bob.id] } })
  // Alice's update isn't replicated

  const removeAliceMsgId = await bob.wiki.update(id, { authors: { remove: [alice.id] } })

  await replicate({ from: alice, to: bob })
  await replicate({ from: bob, to: alice })

  await bob.wiki.update(id, { body: '):' })
    .catch(err => t.match(err.message, /Invalid author/, 'bob can no longer edit'))

  await alice.wiki.update(id, { body: '?!' })
    .catch(err => t.match(err.message, /Invalid author/, 'alice can no longer edit'))

  /* testing #get */
  const expected = {
    key: id,
    type: 'wiki',
    originalAuthor: alice.id,
    recps: null,
    states: [
      {
        key: removeAliceMsgId,
        authors: {
          [alice.id]: [{ start: 0, end: 1 }],
          [bob.id]: [{ start: 0, end: null }]
        },
        body: 'dinosaur notes',
        tombstone: null
      },
      {
        key: removeBobMsgId,
        authors: {
          [alice.id]: [{ start: 0, end: null }]
        },
        body: 'dinosaur notes',
        tombstone: null
      }
    ],
    authors: {
      // Automatically merged on read since there's no conflict
      [alice.id]: [{ start: 0, end: 1 }]
    },
    body: 'dinosaur notes',
    tombstone: null,
    conflictFields: []
  }

  await compareRead(alice, bob, id, expected, t)

  await bob.wiki.update(id, { authors: { add: [alice.id, bob.id] } })
    .catch(err => t.match(err.message, /Invalid author/, 'bob cant fix authors'))

  // NOTE there are currently no "active authors"
  // this is because the combined state of authors from the tips are merged leaving no-one active
  //
  // this means that (normally) no-one can use ssb-crut to author updates
  // the only solution is for both peers to manually publish changes which update the branch
  // that they are allowed to edit on to take it out of a deadlock
  //
  // (see Edge Case A)

  // Let's see if Bob can hack it:
  // Bob manually creates an update on the branch where she has permission.
  const { sequence } = await promisify(bob.getFeedState)(bob.id)
  await promisify(bob.publish)({
    type: 'wiki',
    authors: { [bob.id]: { [sequence]: 1000 } },
    tangles: {
      wiki: {
        root: id,
        previous: [removeAliceMsgId]
      }
    }
  })
  await bob.wiki.update(id, { body: 'success?' })
    .catch(err => t.ok(err, 'bob cannot hack himself access by manually adding himself back in'))

  // Solution:
  // in this case we fall back to the originalAuthor as the person with authorship
  const updateId = await alice.wiki.update(id, { authors: { add: [alice.id] } })
    .catch(t.error) // should be no error here
  t.ok(updateId, 'alice can fix the state because she is the original author')

  alice.close()
  bob.close()
  t.end()
})
