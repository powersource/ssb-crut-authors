const test = require('tape')
const Crut = require('../')
const { SSB, Spec } = require('./helpers')

test('init', t => {
  const ssb = SSB()
  const spec = Spec()

  t.doesNotThrow(
    () => {
      new Crut(ssb, spec) // eslint-disable-line
      new Crut(ssb, spec) // eslint-disable-line
    },
    null,
    'spec can be re-used without throwing'
  )
  // previously we were monkey-patching the spec with spec.props.authors
  // this caused later re-use to throw because props.authors is protected

  ssb.close()

  t.end()
})

test('init - works on spec without props', t => {
  const ssb = SSB()
  const spec = { type: 'wiki' }

  t.doesNotThrow(
    () => {
      new Crut(ssb, spec) // eslint-disable-line
    },
    null,
    'spec without props works'
  )

  ssb.close()

  t.end()
})
